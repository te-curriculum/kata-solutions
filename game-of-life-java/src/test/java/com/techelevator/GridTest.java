package com.techelevator;

import java.util.ArrayList;
import java.util.List;

import org.junit.*;

public class GridTest {

	private Grid grid;
	private List<String> gridLines;
	
	@Before
	public void setupGrid() {
		gridLines = new ArrayList<String>();
		gridLines.add("........");
		gridLines.add("....*...");
		gridLines.add("...**...");
		gridLines.add("........");
		grid = new Grid(gridLines);
	}
	
	@Test
	public void create_grid_from_strings() {
		Assert.assertEquals(4, grid.getRowLength());
		Assert.assertEquals(8,  grid.getColumnLength());
		
		for (int i = 0; i < gridLines.size(); i++) {
			validateRow(gridLines.get(i), i);
		}
		
	}
	
	private void validateRow(String line, int row) {
		for (int i= 0; i < line.length(); i++) {
			if (line.charAt(i) == '.') {
				Assert.assertFalse(grid.getPoint(i, row));
			}
			if (line.charAt(i) == '*') {
				Assert.assertTrue(grid.getPoint(i, row));
			}
		}
	}
	
}
