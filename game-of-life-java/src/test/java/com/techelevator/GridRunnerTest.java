package com.techelevator;

import java.util.ArrayList;
import java.util.List;

import org.junit.*;

public class GridRunnerTest {

	private Runner runner;
	private Grid grid;
	private List<String> gridLines;
	
	@Before
	public void setupGrid() {
		gridLines = new ArrayList<String>();
		gridLines.add("........");
		gridLines.add("....*...");
		gridLines.add("...**...");
		gridLines.add("........");
		grid = new Grid(gridLines);
		runner = new GridRunner();
	}
	
	@Test
	public void sample_grid() {
		Grid end = runner.check(grid);
		Grid expectedGrid = new Grid(gridLines);
		expectedGrid.setAlive(3, 1);
		Assert.assertEquals(expectedGrid, end);
	}
	

	
	
//	@Test 
//	public void test_neighbor_count() {
//		List<String> smallGridLines = new ArrayList<String>();
//		smallGridLines.add(".*.");
//		smallGridLines.add("***");
//		smallGridLines.add(".*.");
//		Grid smallGrid = new Grid(smallGridLines);
//		int midRow = 1;
//		int midCol = 1;
//		GridRunner r = new GridRunner();
//		int neighbors = r.liveNeighbors(smallGrid, midRow, midCol);
//		System.out.println(neighbors);
//	}
	
	
}
