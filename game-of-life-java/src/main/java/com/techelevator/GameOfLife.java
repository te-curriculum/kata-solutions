package com.techelevator;

import java.io.File;
import java.util.Scanner;

import com.techelevator.file.FileGridReader;
import com.techelevator.file.GridReader;

/*
 * http://codingdojo.org/kata/GameOfLife/
 */
public class GameOfLife {

	private Scanner in = new Scanner(System.in);
	
	public void run() {
		GridReader reader = new FileGridReader(getFileFromUser());
		int generations = getNumberOfGenerationsFromUser();
		Grid grid = reader.read();
		Runner runner = new GridRunner();
		
		int i = 1;
		printIteration(grid, i++);
		Grid next = runner.check(grid);
		
		for ( ; i <= generations; i++) {
			next = runner.check(next);
			printIteration(next, i);
		}	
	}
	
	
	private void printIteration(Grid grid, int generation) {
		System.out.println("Generation " + generation);
		for (int row = 0; row < grid.getRowLength(); row++) {
			String line = "";
			for (int col = 0; col < grid.getColumnLength(); col++) {
				line += grid.isAlive(col, row) ? "*" : ".";
			}
			System.out.println(line);
		}
	}
	
	private File getFileFromUser() {
		File file = null;
		while (file == null) {
			System.out.print("Starting Generation File >>>");
			file = new File(in.nextLine());
			if (!file.exists() || !file.isFile()) {
				file = null;
			}
		}

		return file;
	}
	
	private int getNumberOfGenerationsFromUser() {
		System.out.print("Number of generations >>>");
		int generations = in.nextInt();
		in.nextLine();
		return generations;
	}
	
	
	public static void main(String[] args) {
		GameOfLife gameOfLife = new GameOfLife();
		gameOfLife.run();
	}
	
}
