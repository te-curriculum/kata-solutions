package com.techelevator;

import java.util.List;

public class Grid {

	private boolean[][] grid;
	
	public Grid(List<String> lines) {
		grid = new boolean[lines.size()][0];
		for (int j = 0; j < lines.size(); j++) {
			boolean[] gridLine = new boolean[lines.get(j).length()];
			for (int i = 0; i < lines.get(j).length(); i++) {
				if (lines.get(j).charAt(i) == '*') {
					gridLine[i] = true;
				}
			}
			grid[j] = gridLine;
		}
	}
	
	public Grid(Grid other) {
		grid = new boolean[other.getRowLength()][other.getColumnLength()];
	}
	
	public boolean getPoint(int col, int row) {
		return grid[row][col];
	}
	
	public boolean isAlive(int col, int row) {
		return grid[row][col];
	}
	
	public int getColumnLength() {
		return grid[0].length;
	}
	public int getRowLength() {
		return grid.length;
		
	}
	
	public void setAlive(int col, int row) {
		grid[row][col] = true;
	}
	public void setDead(int col, int row) {
		grid[row][col] = false;
	}
	
	@Override
	public boolean equals(Object other) {
		if (other == null || !(other instanceof Grid)) {
			return false;
		}
		Grid otherGrid = (Grid) other;
		
		if (this.getColumnLength() != otherGrid.getColumnLength()) {
			return false;
		}
		
		if (this.getRowLength() != otherGrid.getRowLength()) {
			return false;
		}
		
		for (int row = 0; row < grid.length; row++) {
			for (int col = 0; col < grid[row].length; col++) {
				if (grid[row][col] != otherGrid.getPoint(col, row)) {
					return false;
				}
			}
		}
		
		return true;
	}
}
