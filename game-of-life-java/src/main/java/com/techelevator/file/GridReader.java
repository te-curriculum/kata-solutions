package com.techelevator.file;

import com.techelevator.Grid;

public interface GridReader {

	Grid read();
}
