package com.techelevator;

public class GridRunner implements Runner {

	@Override
	public Grid check(Grid grid) {
		Grid end = new Grid(grid);
		return checkColumn(grid, end, 0, 0);
	}
	
	private Grid checkColumn(Grid start, Grid end, int row, int col) {
		if (col == start.getColumnLength()) {
			return end;
		}
		end = checkRow(start, end, row, col);
		return checkColumn(start, end, row, col + 1);
		
	}
	
	private Grid checkRow(Grid start, Grid end,  int row, int col) {
		if (row == start.getRowLength()) {
			return end;
		}
		if (isAlive(start, row, col)) {
			end.setAlive(col, row);
		} else {
			end.setDead(col, row);
		}
		return checkRow(start, end, row + 1, col);
	}
	
	private boolean isAlive(Grid start,  int row, int col) {
		boolean wasAlive = start.getPoint(col, row);
		int live = liveNeighbors(start, row, col);
		
		if (wasAlive && (live < 2 || live > 3)) {
			return false;
		} else if (!wasAlive && live == 3) {
				return true;
		}
		
		return wasAlive;
	}
	
	
	private int liveNeighbors(Grid start, int row, int col) {
		int cnt = 0;
	    for (int colOffset = -1; colOffset < 2; colOffset++) {
		    for (int rowOffset = -1; rowOffset < 2; rowOffset++) {
		    		int nextCol = col + colOffset;
		    		int nextRow = row + rowOffset;
		    		if ( (nextCol == col && nextRow == row) 
		    				|| nextCol < 0 || nextRow < 0 
		    				|| nextCol >= start.getColumnLength() 
		    				|| nextRow >= start.getRowLength()) {
		    			continue;
		    		}
		    		
		    		if (start.getPoint(nextCol, nextRow)) {
		    			cnt++;
		    		}
		   
		    }
	    }
		return cnt;
	}

	/*            0        1       2
	 * 	   0  [c-1,r-1 ] [c-1] [c-1,r+1]
	 * 	   1  [r-1]        X   [r+1]
	 *     2  [c+1,r-1]  [c+1] [c+1,r+1 ]
	 */
	
	
}
