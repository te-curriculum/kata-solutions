package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.junit.*;

import com.techelevator.grid.Grid;
import com.techelevator.trie.Trie;

public class WordSearcherTest {

	private static String WORD_FILE = "words-list.txt";
	private static final String[] rows = { 	"FHKEFFHD",
											"FDOGIOPV",
											"FJDOOIAQ",
											"AVAJHQRM",
											"CWBXXNRI",
											"AAESFUOF",
											"CUTHESTU",
											"FJJSNJIO" };
	private static final String[] expectedWords = { "CAT", "DOG", "PARROT", "JAVA", "JOG" };
	
	private WordSearcher searcher;
	private Grid grid;
	
	@Before
	public void setup() {
		Trie trie = new Trie();
		for (String word : expectedWords) {
			trie.insert(word);
		}
		searcher = new WordSearcher(trie);
		grid = createGrid();
		
	}
	
	@Test
	public void search_for_words() {
		Set<String> foundWords = searcher.search(grid);
		Assert.assertEquals(foundWords.size(), expectedWords.length);
		Assert.assertTrue(foundWords.containsAll(Arrays.asList(expectedWords)));
		
	}
	
	
	private Grid createGrid() {
		Grid newGrid = new Grid(rows.length, rows[0].length());
		newGrid.addRows(rows);
		return newGrid;
	}
	
	
}
