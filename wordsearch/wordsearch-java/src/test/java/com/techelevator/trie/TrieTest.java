package com.techelevator.trie;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.*;

public class TrieTest {

	private static String WORD_FILE = "words-list.txt";
	
	private Trie trie;
	
	@Before
	public void createTrie() {
		trie = new Trie();
	}
	
	@Test 
	public void add_and_retrieve_word() {
		trie.insert("java");
		Assert.assertTrue(trie.search("java"));
	}
	
	@Test 
	public void word_not_found() {
		trie.insert("java");
		Assert.assertFalse(trie.search("notjava"));
	}
	
	@Test
	public void case_not_used() {
		trie.insert("java");
		Assert.assertTrue(trie.search("JaVa"));
	}
	
	/* Technically this is an integration test */
	@Test
	public void add_and_retrieve_all_words_from_file() {

		List<String> words = insertWordsFromFileIntoTrie();
		for (String word : words) {
			Assert.assertTrue(word + " not found", trie.search(word));
		}
		
	}
	
	@Test
	public void words_not_in_file_not_found() {

		insertWordsFromFileIntoTrie();
		for (String word : getWordsNotInFile()) {
			Assert.assertFalse(word + " found in file", trie.search(word));
		}
		
	}
	
	
	private List<String> insertWordsFromFileIntoTrie() {
		List<String> words = null;
		try {
			words = getWordsFromFile();
		} catch (FileNotFoundException e) {
			Assert.fail("File " + WORD_FILE + " not found");
		}
		
		for (String word : words) {
			trie.insert(word);
		}
		
		return words;
	}
	
	private List<String> getWordsFromFile() throws FileNotFoundException {
		List<String> words = new ArrayList<String>();
		
		try (Scanner file = new Scanner(new File(WORD_FILE))) {
			while (file.hasNext()) {
				String word = file.next().replaceAll("\\n\\t ]",  "");
				if (word.length() > 0) {
					words.add(word);
				}
			}
		}
		
		return words;
	}
	
	private String[] getWordsNotInFile() {
		return new String[] { "a", "accordings", "achooing", "gazingly", "pewing", "zooing", "", " ", "zzzz", "\\n", "\\t"};
	}
	
}
