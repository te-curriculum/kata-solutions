package com.techelevator.grid;

import org.junit.*;

import com.techelevator.grid.Grid;

// TODO: Need tests for Exceptions
public class GridTest {

	private Grid grid;
	private static final String[] rows = { 	"FHKEFFHD",
											"FDOGIOPV",
											"FJDKOIAQ",
											"FJEIHQRM",
											"CWBXXNRI",
											"AAESFUOF",
											"CUTHESTU",
											"FJJSNJIO" };
	@Before
	public void setup() {
		grid = new Grid(rows.length, rows[0].length());
	}
	
	@Test
	public void add_and_get_rows() {
		grid.addRows(rows);
		
		for (int i = 0; i < grid.rows(); i++) {
			Assert.assertEquals(grid.getRow(i), getDelimitedRow(rows[i]));
		}
		
	}
	
	@Test
	public void rows_correct_number() {
		grid.addRows(rows);
		Assert.assertEquals(rows.length, grid.rows());
	}
	
	@Test
	public void columns_correct_number() {
		grid.addRows(rows);
		Assert.assertEquals(rows[0].length(), grid.columns());
	}
	
	
	private String getDelimitedRow(String row) {
		String[] split = row.split("");
		return String.join(" ", split);
	}
}
