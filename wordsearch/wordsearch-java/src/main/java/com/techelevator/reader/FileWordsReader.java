package com.techelevator.reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.techelevator.trie.Trie;

public class FileWordsReader implements WordsReader {

	private File wordsFile;
	
	public FileWordsReader(File wordsFile) {
		this.wordsFile = wordsFile;
	}

	@Override
	public Trie read() {
		List<String> words = null;
		try {
			words = getWordsFromFile();
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		
		Trie trie = new Trie();
		
		for (String word : words) {
			trie.insert(word);
		}
		
		return trie;
	}
	
	private List<String> getWordsFromFile() throws FileNotFoundException {
		List<String> words = new ArrayList<String>();
		
		try (Scanner file = new Scanner(wordsFile)) {
			while (file.hasNext()) {
				String word = file.next().replaceAll("\\n\\t ]",  "");
				if (word.length() > 0) {
					words.add(word);
				}
			}
		}
		
		return words;
	}
	
	
}
