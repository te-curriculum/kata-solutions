package com.techelevator.reader;

import com.techelevator.trie.Trie;

public interface WordsReader {
	
	Trie read();
}
