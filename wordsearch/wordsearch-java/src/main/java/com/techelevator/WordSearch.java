package com.techelevator;

import java.io.File;
import java.util.Scanner;
import java.util.Set;

import com.techelevator.grid.Grid;
import com.techelevator.grid.GridGenerator;
import com.techelevator.reader.FileWordsReader;
import com.techelevator.reader.WordsReader;
import com.techelevator.trie.Trie;

public class WordSearch {

	private static final Scanner in = new Scanner(System.in);

	public void run() {
		GridGenerator generator = new GridGenerator();
		
		System.out.println("Welcome to Word Search");
		File wordListPath = getWordListFileFromUser();
		int gridSize = getGridSizeFromUser();
		
		WordsReader reader = new FileWordsReader(wordListPath);
		Trie words = reader.read();
		Grid grid = generator.create(gridSize);
		printGrid(grid);
		
		WordSearcher searcher = new WordSearcher(words);
		
		showFoundWords(searcher.search(grid));
		
	}
	
	
	private void showFoundWords(Set<String> foundWords) {
		System.out.println("Words found:");
		
		for (String word: foundWords) {
			System.out.println(word);
		}
	}
	
	private File getWordListFileFromUser() {
		File file = null;
		while (file == null) {
			System.out.print("Word List File >>>");
			String path = in.nextLine();
			file = new File(path);
			if (!file.exists() || !file.isFile()) {
				System.out.println("File not found!");
				file = null;
			}
		}
		return file;
	}
	
	private void printGrid(Grid grid) {
		System.out.println();
		for (int i = 0; i < grid.rows(); i++) {
			System.out.println(grid.getRow(i));
		}
		System.out.println();
	}
	
	private int getGridSizeFromUser() {
		System.out.print("Size of grid >>>");
		return in.nextInt();
	}
	
	
	public static void main(String[] args) {
		new WordSearch().run();
	}
	
}
