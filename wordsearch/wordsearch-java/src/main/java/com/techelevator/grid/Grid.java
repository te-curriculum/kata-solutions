package com.techelevator.grid;

public class Grid {
	
	private char[][] table;

	public Grid(int columns, int rows) {
		table = new char[rows][columns];
	}
	
	public void addRows(String[] rows) {
		if (rows.length != table.length) {
			throw new IllegalArgumentException("Incorrect number of rows!");
		}
		int rowNum = 0;
		for (String row : rows) {
			if (row.length() != table[rowNum].length) {
				throw new IllegalArgumentException("String is not correct length for columns in row " + rowNum);
			}
			
			String rowUpper = row.toUpperCase();
			for (int i = 0; i < rowUpper.length(); i++) {
				table[rowNum][i] = rowUpper.charAt(i);
			}
			
			rowNum++;
		}
	}
	
	public int rows() {
		return table.length;
	}
	
	public int columns() {
		return table[0].length;
	}
	
	//TODO: Need Unit Test
	public char getCharAtPoint(int row, int col) {
		return table[row][col];
	}
	
	public String getRow(int num) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < table[num].length; i++) {
			sb.append(table[num][i]);
			if (i < table[num].length - 1) {
				sb.append(" ");
			}
		}
		return sb.toString();
	}
	
}
