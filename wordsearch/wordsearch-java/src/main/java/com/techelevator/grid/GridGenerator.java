package com.techelevator.grid;

import java.util.Random;

public class GridGenerator {

	private static final Random random = new Random();;
	
	//TODO: Need Unit Tests
	public Grid create(int size) {
		Grid grid = new Grid(size, size);
		
		String[] rows = new String[size];
		for (int r = 0; r < size; r++) {
			rows[r] = createRow(size);
		}
		grid.addRows(rows);
		return grid;
	}
	
	private String createRow(int length) {
		int lowUppercase = 65;
		int highUppercase = 90;
		
		StringBuffer sb = new StringBuffer(length);
		
		for (int i = 0; i < length; i++) {
			int nextChar = lowUppercase + (int)(random.nextFloat() * (highUppercase - lowUppercase + 1));
			sb.append((char) nextChar);
		}
		
		return sb.toString();
	}
	
	
}
