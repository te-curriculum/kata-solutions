package com.techelevator;

import java.util.Set;
import java.util.TreeSet;

import com.techelevator.grid.Grid;
import com.techelevator.trie.Trie;

public class WordSearcher {

	private Trie wordList;
	
	public WordSearcher(Trie wordList) {
		this.wordList = wordList;
	}
	
	public Set<String> search(Grid grid) {
		Set<String> foundWords = new TreeSet<String>();
		
		for (int row = 0; row < grid.rows(); row++) {
			for (int col = 0; col < grid.columns(); col++) {
				foundWords = searchFromPosition(grid, row, col, foundWords);

			}
		}
		
		return foundWords;
	}
	
	private Set<String> searchFromPosition(Grid grid, int row, int col, Set<String> foundWords) {
		
		// Check Forward and back
		checkDirection(grid, row, col, 0, 1, foundWords);
		checkDirection(grid, row, col, 0, -1, foundWords);
		// Check Down and Up
		checkDirection(grid, row, col, 1, 0, foundWords);
		checkDirection(grid, row, col, -1, 0, foundWords);
		// Check Diagonals
		checkDirection(grid, row, col, 1, -1, foundWords);
		checkDirection(grid, row, col, 1, 1, foundWords);
		checkDirection(grid, row, col, -1, 1, foundWords);
		checkDirection(grid, row, col, -1, -1, foundWords);
		
		return foundWords;
	}
	


	private void checkDirection(Grid grid, int row, int col, int rowOffset, int colOffset, Set<String> foundWords) {
		
		String possibleWord = "";
		while (row < grid.rows() && row >= 0 && col < grid.columns() && col >= 0) {

			possibleWord += String.valueOf(grid.getCharAtPoint(row, col));
			if (possibleWord.length() > 2) {
				if (wordList.search(possibleWord)) {
					foundWords.add(possibleWord);
				}
			}
			
			row += rowOffset;
			col += colOffset;
		}
	}
	
}
