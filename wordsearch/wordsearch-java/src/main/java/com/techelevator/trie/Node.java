package com.techelevator.trie;

import java.util.LinkedList;

public class Node {

	private char value;
	private boolean end;
	private LinkedList<Node> children;
	private int count;
	
	public Node(char c) {
		children = new LinkedList<Node>();
		end = false;
		value = c;
		count = 0;
	}
	
	public Node getChild(char c) {
		if (children != null) {
			for (Node child: children) {
				if (child.getValue() == c) {
					return child;
				}
			}
		}
		return null;
	}
	
	public char getValue() {
		return value;
	}
	
	public boolean isEnd() {
		return end;
	}
	
	public void setEnd() {
		this.end = true;
	}
	
	public int getCount() {
		return count;
	}
	
	public void incrementCount() {
		count++;
	}
	
	public void addChild(Node child) {
		children.add(child);
	}
}
