package com.techelevator.trie;

/*
 * https://en.wikipedia.org/wiki/Trie
 */
public class Trie {

	private Node root;
	
	public Trie() {
		root = new Node(' ');
	}
	
	public void insert(String word) {
		Node current = root;
		
		if (search(word)) {
			return;
		}
		
		for (char ch : word.toUpperCase().toCharArray()) {
			Node child = current.getChild(ch);
			if (child != null) {
				current = child;
			} else {
				current.addChild(new Node(ch));
				current = current.getChild(ch);
			}
			current.incrementCount();
		}
		current.setEnd();
	}
	
	public boolean search(String word) {
		Node current = root;
		for (char ch : word.toUpperCase().toCharArray()) {
			if (current.getChild(ch) == null) {
				return false;
			}
			current = current.getChild(ch);
		}
		return current.isEnd();
	}
	
}
