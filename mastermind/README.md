# Mastermind Kata

Mastermind if a code-breaking game for two players.  The codemaker creates a code of 4 colors.  The codebreaker player makes guesses at the code by creating a combination of 4 colors.  For each turn the code setter tells the code breaker the number of correct colors in the correct location in their guess and the number of correct color but in incorrect locations in their guess.  

The codebreaker has a set number of turns to guess the code.  

Write a Mastermind Game where the computer takes the role of the Codemaker and the player takes the role of the Codebreaker.

## Application Requirements

    1. Create a random code from the allowed colors.  Duplicate colors are alllowed, but blank spaces are not permitted.  
    2. Take a guess from the player that is an set of 4 colors
    3. Return the number of well placed and misplaced colors in the guess
    4. Allow the player to guess for X number of turns
    5. If the player correctly guesses the code, the player wins
    6. If the player is not able to guess the code in the number of allowed turns then the computer wins
    7. On a win or lose conditional reveal the code to the player   

### Bonus Requirements

    1. Personalize the game with the players name
    2. Allow the player to set the difficulty
    3. Allow the player to correct their choice before submitting it (if using a web based UI)
    4. Allow the player to reset the game at any time
    5. Allow the player to play multiple games
    6. If multiple games allowed, track the number of player vs computer wins
    7. Give a player a way to view the rules of the game at anytime wthout interupting a game in progress

## Rules

    1. any number of colors can be used.  Traditionally the game is played with 6 colors - more colors increases the difficulty while less colors decrease the difficulty.
    2. The code can be any length. Traditionally a code of 4 colors is used.  A longer code greatly increases the difficulty and a shorter code greatly decreases it.  
    3. any number of guesses can be allowed for the codebreaker.  Traditionally the game allows 10 guesses.  More guesses decreases the difficulty, while fewer increases it difficutly. 
    4.  Well placed colors are represented by a black "peg".  Misplaced colors are represented by a white "peg".  No "peg" is set for colors that are not part of the code.  Well placed is defined as a correct color in the correct position.  Misplaced is defined as a correct color in an incorrect position. 
        * Pegs can be repsented as numbers or symbols
            Number Example:  [well-placed, misplaced]  [2, 1]
            Symbol Example:  [*, *, #]

## Test Cases

    Code: [blue, red, green, orange]
    Guess: [yellow, pink, purple, yellow]
    Return: [0,0 ]  or []

    Code: [blue, red, green, orange]
    Guess: [blue, blue, orange, yellow]
    Return: [1,1]  or [*, #]

    Code: [blue, red, green, orange]
    Guess: [blue, red, orange, yellow]
    Return: [2,1]  or [*, *, #]

    Code: [blue, red, green, orange]
    Guess: [blue, orange, orange, green]
    Return: [1,3]  or [*, #, #, #]

    Code: [blue, red, green, orange]
    Guess: [blue, red, orange, green]
    Return: [2,2]  or [*, *, #, #]

    Code: [blue, red, green, orange]
    Guess: [red, green, orange, blue]
    Return: [0,4]  or [#, #, #, #]

    Code: [blue, red, green, orange]
    Guess: [blue, red, green, orange]
    Return: [4,0]  or [*, *, *, *]  <-- win

## Language Choice

This kata can be completed in any language and the game can be played in either terminal or a web based UI.  

## Hints
 * Start by detecting well placed colors
 * Detecting misplaced colors is about counting them
 * Take a TDD approach.  Focusing on 1 aspect of the result at a time.

Mastermind Wiki: https://en.wikipedia.org/wiki/Mastermind_(board_game)
Playable web version: https://www.archimedes-lab.org/mastermind.html

