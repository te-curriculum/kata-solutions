package com.techelevator;

public class TurnTaker {

	private String[] code;
	private int numberOfTurns;
	private int turn = 1;
	
	public TurnTaker(int numberOfTurns, String[] code) {
		this.numberOfTurns = numberOfTurns;
		this.code = code;
	}
	
	public int getTurn() {
		return turn;
	}
	
	public boolean hasTurnsRemaining() {
		return numberOfTurns - turn >= 0;
	}
	
	public GuessResult takeTurn(String[] guess) {
		turn++;
		String[] codeClone = code.clone();
		String[] guessClone = guess.clone();
		int wellPlaced = determineWellPlaced(guessClone, codeClone);
		int outOfPlace =determineOutOfPlace(guessClone, codeClone);
		return new GuessResult(wellPlaced, outOfPlace);
	}
	

	private int determineOutOfPlace(String[] guess, String[] codeClone) {
		int cnt = 0;
		
		for (int i = 0; i < guess.length; i++) {
			if (guess[i].length() > 0) {
				for (int j = 0; j < codeClone.length; j++) {
					if (codeClone[j].length() > 0 && guess[i].equalsIgnoreCase(codeClone[j])) {
						cnt++;
						codeClone[j] = "";
						guess[i] = "";
					}
				}
			}
		}
		
		return cnt;
	}
	
	
	private int determineWellPlaced(String[] guess, String[] codeClone) {
		int cnt = 0;
		for (int i = 0; i < guess.length; i++) {
			if (guess[i].equalsIgnoreCase(codeClone[i])) {
				codeClone[i] = "";
				guess[i] = "";
				cnt++;
			} 
		}
		return cnt;
	}
	
}
