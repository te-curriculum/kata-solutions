package com.techelevator;

import java.util.Arrays;
import java.util.Random;

public class Code {

	private Random rand;
	
	private int codeLength;
	private String[] colors;
	private String[] code;
	
	public Code(String[] colors, int codeLength) {
		this.codeLength = codeLength;
		this.colors = colors;
		this.rand = new Random();
	}

	public String[] create() {
		code = new String[this.codeLength];
		for (int i = 0; i < code.length; i++) {
			int colorIndex = rand.nextInt(colors.length);
			code[i] = colors[colorIndex];
		}
		return code;
	}
	
	public String[] getCode() {
		return code;
	}
	
	public int length() {
		return codeLength;
	}
	
	@Override
	public String toString() {
		return "[" + String.join(" ", code) + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(code);
		result = prime * result + codeLength;
		result = prime * result + Arrays.hashCode(colors);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Code other = (Code) obj;
		if (!Arrays.equals(code, other.code))
			return false;
		if (codeLength != other.codeLength)
			return false;
		if (!Arrays.equals(colors, other.colors))
			return false;
		return true;
	}
	

}
