package com.techelevator;

public class GuessResult {

	private int wellPlaced;
	private int outOfPlace;
	
	public GuessResult(int wellPlaced, int outOfPlace) {
		this.wellPlaced = wellPlaced;
		this.outOfPlace = outOfPlace;
	}

	public int getWellPlaced() {
		return wellPlaced;
	}

	public int getOutOfPlace() {
		return outOfPlace;
	}
	
}
