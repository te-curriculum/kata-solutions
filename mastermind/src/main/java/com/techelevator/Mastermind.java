package com.techelevator;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Mastermind {

	private static final Scanner in = new Scanner(System.in);
	
	private static final String[] colors = { "Red", "Blue", "Green", "Yellow", "Orange", "Purple" };
	private static final int codeLength = 4;
	private static final int numberOfTurns = 10;
	
	public void execute() {
		startGame();
		play();
	}
	
	private void play() {
		Code code = new Code(colors, codeLength);
		
		code.create();
		TurnTaker turnTaker = new TurnTaker(numberOfTurns, code.getCode());
		
		boolean hasWon = false;
		
		while (turnTaker.hasTurnsRemaining() && !hasWon) {
			String[] guess = getPlayerGuess(turnTaker.getTurn());
			GuessResult result = turnTaker.takeTurn(guess);
			
			showTurnResult(guess, result);
			hasWon = isWin(result);
		}
		
		showGameResult(hasWon, code);
	}
	
	private void showGameResult(boolean hasWon, Code code) {
		
		System.out.println();
		
		if (hasWon) {
			System.out.println("You correctly guessed the Code!");
		} else {
			System.out.println("You failed to guess the code in " + numberOfTurns + " tries.");
			
		}
		System.out.println("The code was " + code);
	}
	
	
	private void showTurnResult(String[] guess, GuessResult result) {
		System.out.print("[" +  Arrays.asList(guess).stream().collect(Collectors.joining(" ")) + "]");
		System.out.println(" [" + result.getWellPlaced() + ", " + result.getOutOfPlace() + "]");
	}
	
	private boolean isWin(GuessResult result) {
		return result.getWellPlaced() == codeLength;
	}
	
	private void startGame() {
		System.out.println("Welcome to Mastermind!");
		System.out.println();
		System.out.println("The code is " + codeLength + " colors.");
		System.out.println("You have " + numberOfTurns + " tries to guess the code!");
		System.out.println("Take a guess by entering a 4 colors seperated by spaces: Red Blue Red Green");
		System.out.println("The result will tell you [The number of well placed colors, The number of out of place colors]");
		System.out.println();
		System.out.println("Allowed colors: " + Arrays.asList(colors).stream().collect(Collectors.joining(" ")));
		System.out.println();
	}
	
	private String[] getPlayerGuess(int turn) {
		String[] guess = null;
		
		while (guess == null) {
			System.out.print("Guess " + turn + " of " + numberOfTurns + " >>>");
			String userInput = in.nextLine();
			guess =  userInput.split(" "); 
			guess = isGuessValid(guess) ? guess: null;
		}
		
		return guess;
	}
	
	private boolean isGuessValid(String[] guess) {
		if (guess.length != codeLength) {
			System.out.println("Guess does not contain the right number of colors!");
			return false;
		}
		List<String> listOfColors = Arrays.asList(colors);
		for (String color: guess) {
			if (!listOfColors.stream().anyMatch(color::equalsIgnoreCase)) {
				System.out.println(color + " is not a valid color for this game!");
				return false;
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		new Mastermind().execute();
	}
}
