package com.techelevator;

import org.junit.*;

public class TurnTakerTest {

	private TurnTaker turnTaker;
	
	private static final int TURN_COUNT = 10;
	private static final String[] TEST_CODE = {"Blue", "Red", "Green", "Orange" };
	
	@Before
	public void setup() {
		turnTaker = new TurnTaker(10, TEST_CODE);
	}
	
	@Test
	public void turn_count() {
		Assert.assertEquals("Incorrect first turn count", 1, turnTaker.getTurn());
		
		turnTaker.takeTurn(TEST_CODE);
		Assert.assertEquals("Incorrect second turn count", 2, turnTaker.getTurn());
		
		turnTaker.takeTurn(TEST_CODE);
		turnTaker.takeTurn(TEST_CODE);
		Assert.assertEquals("Incorrect fourth turn count", 4, turnTaker.getTurn());
	}
	
	@Test
	public void has_remaining() {
		Assert.assertTrue("No turns left after 0 turns", turnTaker.hasTurnsRemaining());
		
		turnTaker.takeTurn(TEST_CODE);
		Assert.assertTrue("No turns left after 1 turn", turnTaker.hasTurnsRemaining());
		
		turnTaker.takeTurn(TEST_CODE);
		turnTaker.takeTurn(TEST_CODE);
		Assert.assertTrue("No turns left after 3 turns", turnTaker.hasTurnsRemaining());
	}
	
	@Test
	public void allows_correct_number_of_returns() {
		
		for (int i = 1; i < TURN_COUNT; i++) {
			turnTaker.takeTurn(TEST_CODE);
			Assert.assertTrue("No turns left after " + i + " turns", turnTaker.hasTurnsRemaining());
		}
		turnTaker.takeTurn(TEST_CODE);
		Assert.assertFalse("Turns remaining after maximum turns", turnTaker.hasTurnsRemaining());
	}
	
	@Test
	public void colors_not_found() {
		String[] guess = {"yellow", "pink", "purple", "yellow" };
		GuessResult result = turnTaker.takeTurn(guess);
		Assert.assertEquals(0, result.getWellPlaced());
		Assert.assertEquals(0, result.getOutOfPlace());
	}
	
	@Test
	public void one_well_placed() {
		String[] guess = {"blue", "pink", "purple", "yellow" };
		GuessResult result = turnTaker.takeTurn(guess);
		Assert.assertEquals(1, result.getWellPlaced());
		Assert.assertEquals(0, result.getOutOfPlace());
	}
	
	@Test
	public void one_out_of_place() {
		String[] guess = {"yellow", "blue", "purple", "yellow" };
		GuessResult result = turnTaker.takeTurn(guess);
		Assert.assertEquals(0, result.getWellPlaced());
		Assert.assertEquals(1, result.getOutOfPlace());
	}
	
	@Test
	public void one_well_placed_and_one_out_of_place() {
		String[] guess = {"blue", "blue", "orange", "yellow" };
		GuessResult result = turnTaker.takeTurn(guess);
		Assert.assertEquals(1, result.getWellPlaced());
		Assert.assertEquals(1, result.getOutOfPlace());
	}
	
	@Test
	public void two_well_placed_and_one_out_of_place() {
		String[] guess = {"blue", "red", "orange", "yellow" };
		GuessResult result = turnTaker.takeTurn(guess);
		Assert.assertEquals(2, result.getWellPlaced());
		Assert.assertEquals(1, result.getOutOfPlace());
	}
	
	@Test
	public void one_well_placed_and_three_out_of_place() {
		String[] guess = {"blue", "orange", "red", "green" };
		GuessResult result = turnTaker.takeTurn(guess);
		Assert.assertEquals(1, result.getWellPlaced());
		Assert.assertEquals(3, result.getOutOfPlace());
	}
	
	@Test
	public void two_well_placed_and_two_out_of_place() {
		String[] guess = {"blue", "red", "orange", "green" };
		GuessResult result = turnTaker.takeTurn(guess);
		Assert.assertEquals(2, result.getWellPlaced());
		Assert.assertEquals(2, result.getOutOfPlace());
	}
	
	@Test
	public void four_out_of_place() {
		String[] guess = {"red", "green", "orange", "blue" };
		GuessResult result = turnTaker.takeTurn(guess);
		Assert.assertEquals(0, result.getWellPlaced());
		Assert.assertEquals(4, result.getOutOfPlace());
	}
	
	@Test
	public void four_well_placed() {
		String[] guess = {"blue", "red", "green", "orange" };
		GuessResult result = turnTaker.takeTurn(guess);
		Assert.assertEquals(4, result.getWellPlaced());
		Assert.assertEquals(0, result.getOutOfPlace());
	}
}
