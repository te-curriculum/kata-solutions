package com.techelevator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.hamcrest.core.IsEqual;
import org.hamcrest.core.IsNot;
import org.junit.*;


public class CodeTest {

	private int codeLength;
	private String[] colors;
	private Code code;
	
	@Before
	public void setup() {
		colors = new String[]{"Red", "Blue", "Yellow", "Green", "Orange", "Purple"};
		codeLength = 4;
		code = new Code(colors, codeLength);
	}
	
	@Test
	public void get_code_length() {
		Assert.assertEquals(codeLength, code.length());
	}
	
	@Test
	public void create_one_code() {
		String[] generatedCode = code.create();
		Assert.assertEquals("Invalid code length", 4, generatedCode.length);
		
		for (String color: generatedCode) {
			Assert.assertTrue("Invalid code value: " + color, Arrays.stream(colors).anyMatch(color::equals));
		}
	}
	
	@Test
	public void all_colors_are_used_for_codes() {
		List<Boolean> found = Arrays.asList(new Boolean[colors.length]);
		List<String> colorsAsList = Arrays.asList(colors);
		boolean foundAll = false;
		int chances = 1000;
		
		while (!foundAll && chances > 0) {
			String[] generatedCode = code.create();
			chances--;
			for (String color: generatedCode) {
				int index = colorsAsList.indexOf(color);
				found.set(index, true);
			}
			if (found.stream().allMatch(Boolean.TRUE::equals)) {
				foundAll = true;
			}
		}
		Assert.assertTrue("Did not find all colors after " + chances + " attempts", foundAll);
	}
	
	@Test
	public void creates_unique_codes() {
		String[] codeOne = code.create();
		String[] codeTwo = code.create();
		Assert.assertThat(codeOne, IsNot.not(IsEqual.equalTo(codeTwo)));
		Assert.assertNotEquals(codeOne, codeTwo);
	}
	
	
	@Test
	public void toString_override() {
		String[] codeOne = code.create();
		StringBuilder sb = new StringBuilder();
		for (String color : codeOne) {
			sb.append("[");
			sb.append(color);
			sb.append("]");
		}
		Assert.assertEquals(sb.toString(), code.toString());
	}
	
	
}
